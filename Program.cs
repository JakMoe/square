﻿using System;

namespace Square
{
    class Square
    {
        public static int ParseToInt(string num)
        {
            int number = Int32.Parse(num);
            return number;
        }
        public static void ProgramEnd()
        {
            Console.WriteLine("Press R to restart or any other key to quit: ");
            string userInput = Console.ReadLine();
            string answer = userInput.ToLower();

                if (answer == "r")
            {
                RunProgram();
            }
            else
            {
                Console.WriteLine("Cya Nerd!");
            }
                    
            
        }
        public static void RunProgram()
        {
            Console.WriteLine("Width of square:");
            string input1 = Console.ReadLine();
            Console.WriteLine("Height of square:");
            string input2 = Console.ReadLine();
            Console.WriteLine("Choose one character to build square with:");
            string input3 = Console.ReadLine();

            int num1 = ParseToInt(input1);
            int num2 = ParseToInt(input2);

            for (int i = 0; i < num1 + 1; i++)
            {
                string tempString = "";
                for (int j = 0; j <= num2; j++)
                {
                    if (i == 0 || i == num1 || j == 0 || j == num2)
                    {

                        tempString = tempString + input3;
                    }
                    else
                    {
                        tempString = tempString + " ";
                    }

                }
                Console.WriteLine(tempString);
            }
            ProgramEnd();

        }
        static void Main(string[] args)
        {
            RunProgram();
        }
    }
       
}
