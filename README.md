# Square

Console application that produces a square/rectangle
with size that the user specifies.
## Getting Started

Open it in Visual studio, hit run.

## Authors

* **Jakob Hauge Moe** - *Initial work* - [JakMoe](https://gitlab.com/JakMoe)
